package pl.edu.wat.wcy.jfk;

import pl.edu.wat.wcy.jfk.resource.*;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws ParseException, FileNotFoundException {

        String exampleTimespanToken1 = "01:04:00:23:33:00";
        String exampleTimespanToken2 = "00:06:00:00:00:00";

        String exampleDateToken1 = "04/10/1996";
        String exampleDateToken2 = "25/07/1999";

        String exampleDateTimeToken1 = "04/10/1992#15:15:15";
        String exampleDateTimeToken2 = "04/10/1941#04:15:23";


        DateToken dateToken1 = new DateToken(exampleDateToken1);
        DateToken dateToken2 = new DateToken(exampleDateToken2);

        Timespan time1 = new Timespan(exampleTimespanToken1);
        Timespan time2 = new Timespan(exampleTimespanToken2);

        DateTimeToken timedate1= new DateTimeToken(exampleDateTimeToken1);
        DateTimeToken timedate2= new DateTimeToken(exampleDateTimeToken2);

        Timespan resultOfAdd = Calculator.operationOnTimespans(time1, time2, "+");
        Timespan resultOfSub = Calculator.operationOnTimespans(time1, time2, "-");
        Timespan resultTime1 = Calculator.differenceBetweenDateTime(timedate1,timedate2);

        DateTimeToken resultTimeAdd = Calculator.calculateDateTimeAndTimespan("+",time1,timedate1);

        DateToken resultOfDateAdd = Calculator.calculateDateAndTimespan("+",time1,dateToken1);
        DateToken resultOfDateSub = Calculator.calculateDateAndTimespan("-",time1,dateToken1);

        System.out.println(Calculator.calculateDifferenceBetweenDates(dateToken1, dateToken2).printValidTimespan());
        System.out.println(resultOfAdd.printValidTimespan());
        System.out.println(resultOfSub.printValidTimespan());
        System.out.println(resultOfDateAdd.printValidStringOfDate());
        System.out.println(resultOfDateSub.printValidStringOfDate());
        System.out.println(resultTime1.printValidTimespan());
        System.out.println(resultTimeAdd.printValidDateTime());



    }
}

