package pl.edu.wat.wcy.jfk.resource.DTO;

import pl.edu.wat.wcy.jfk.resource.DateTimeToken;
import pl.edu.wat.wcy.jfk.resource.DateToken;
import pl.edu.wat.wcy.jfk.resource.Timespan;

/**
 * Created by Krzysztof Kopydłowski on 03-Dec-17.
 */
public class AllTimesDTO {

  private DateTimeToken dateTimeToken = null;
  private DateToken dateToken = null;
  private Timespan timespan = null;

  public DateTimeToken getDateTimeToken() {
    return dateTimeToken;
  }

  public void setDateTimeToken(DateTimeToken dateTimeToken) {
    this.dateTimeToken = dateTimeToken;
  }

  public DateToken getDateToken() {
    return dateToken;
  }

  public void setDateToken(DateToken dateToken) {
    this.dateToken = dateToken;
  }

  public Timespan getTimespan() {
    return timespan;
  }

  public void setTimespan(Timespan timespan) {
    this.timespan = timespan;
  }

  public void setAllAttributesNull() {
    this.dateTimeToken = null;
    this.dateToken = null;
    this.timespan = null;
  }
}
