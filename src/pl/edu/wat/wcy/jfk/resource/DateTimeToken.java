package pl.edu.wat.wcy.jfk.resource;

import java.time.LocalDateTime;

public class DateTimeToken {

    private LocalDateTime datetime;

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }


    //DD/MM/YYYY#HH:mm:ss
    public DateTimeToken(String token){
        String parts[] = token.split("#");
        String partsTime[] = parts[1].split(":");
        String partsDate[] = parts[0].split("/");
        this.datetime=LocalDateTime.of(Integer.parseInt(partsDate[2]),Integer.parseInt(partsDate[1]),Integer.parseInt(partsDate[0]),
                Integer.parseInt(partsTime[0]),Integer.parseInt(partsTime[1]),Integer.parseInt(partsTime[2]));
    }

    public String printValidDateTime() {
        String seconds = String.valueOf(this.datetime.getSecond());
        String minutes = String.valueOf(this.datetime.getMinute());
        String hour = String.valueOf(this.datetime.getHour());
        String day = String.valueOf(this.datetime.getDayOfMonth());
        String month = String.valueOf(this.datetime.getMonthValue());
        String year = String.valueOf(this.datetime.getYear());
        return day+"/"+month+"/"+year+"#"+hour+":"+minutes+":"+seconds;
    }

    public DateTimeToken(LocalDateTime datetime) {
        this.datetime = datetime;
    }
}
