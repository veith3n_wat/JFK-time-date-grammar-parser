package pl.edu.wat.wcy.jfk.resource;


import java.time.Duration;
import java.time.Period;

public class Timespan {

    private Duration timespan;

    private Period period;

    private String token;


    public Timespan(String token) {
        String[] splits = token.split(":");
        Long numberOfSeconds = Long.parseLong(splits[3]) * 3600 + Long.parseLong(splits[4]) * 60 + Long.parseLong(splits[5]);
        timespan = Duration.ofSeconds(numberOfSeconds);
        period = Period.of(Integer.parseInt(splits[0]),Integer.parseInt(splits[1]),Integer.parseInt(splits[2]));
        this.token=token;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timespan() {

    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Duration getTimespan() {
        return timespan;
    }


    public void setTimespan(Duration timespan) {
        this.timespan = timespan;
    }

    public String printValidTimespan() {
        Long numberOfSeconds = this.timespan.getSeconds();
        Long numberOfHours = numberOfSeconds / 3600;
        numberOfSeconds -= numberOfHours * 3600;
        Long numberOfMinutes = numberOfSeconds / 60;
        numberOfSeconds -= numberOfMinutes * 60;
        return this.period.getYears() + ":" + this.period.getMonths() + ":" + this.period.getDays() + ":" + numberOfHours.toString() + ":" + numberOfMinutes.toString() + ":" + numberOfSeconds.toString();
    }

    public Timespan(Duration timespan, Period period) {
        this.timespan = timespan;
        this.period = period;
    }
}
