package pl.edu.wat.wcy.jfk.resource;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class Calculator {


    static public DateToken calculateDateAndTimespan(String operation, Timespan timespan, DateToken date) {
        LocalDateTime simpleDate = date.getDate();
        switch (operation) {
            case "+": {
                simpleDate = simpleDate.plusDays(timespan.getPeriod().getDays());
                simpleDate = simpleDate.plusMonths(timespan.getPeriod().getMonths());
                simpleDate = simpleDate.plusYears(timespan.getPeriod().getYears());
                return new DateToken(simpleDate);
            }
            case "-": {
                simpleDate = simpleDate.minusDays(timespan.getPeriod().getDays());
                simpleDate = simpleDate.minusMonths(timespan.getPeriod().getMonths());
                simpleDate = simpleDate.minusYears(timespan.getPeriod().getYears());
                return new DateToken(simpleDate);
            }
        }
        return null;
    }

    static public DateTimeToken calculateDateTimeAndTimespan(String operation, Timespan timespan, DateTimeToken date) {
        LocalDateTime simpleDate = date.getDatetime();
        String[] splits = timespan.getToken().split(":");
        int hours = Integer.parseInt(splits[3]);
        int minutes = Integer.parseInt(splits[4]);
        int seconds = Integer.parseInt(splits[5]);
        switch (operation) {
            case "+": {
                simpleDate = simpleDate.plusDays(timespan.getPeriod().getDays());
                simpleDate = simpleDate.plusMonths(timespan.getPeriod().getMonths());
                simpleDate = simpleDate.plusYears(timespan.getPeriod().getYears());
                simpleDate = simpleDate.plusHours(hours);
                simpleDate = simpleDate.plusMinutes(minutes);
                simpleDate = simpleDate.plusSeconds(seconds);
                return new DateTimeToken(simpleDate);
            }
            case "-": {
                simpleDate = simpleDate.minusDays(timespan.getPeriod().getDays());
                simpleDate = simpleDate.minusMonths(timespan.getPeriod().getMonths());
                simpleDate = simpleDate.minusYears(timespan.getPeriod().getYears());
                simpleDate = simpleDate.minusHours(hours);
                simpleDate = simpleDate.minusMinutes(minutes);
                simpleDate = simpleDate.minusSeconds(seconds);
                return new DateTimeToken(simpleDate);
            }
        }
        return null;
    }

    //data 1 is before date 2
    static public Timespan differenceBetweenDateTime(DateTimeToken date1, DateTimeToken date2) {
        if (date1.getDatetime().isAfter(date2.getDatetime())) {
            DateTimeToken tmp = date1;
            date1 = date2;
            date2 = tmp;
        }
        LocalDateTime fromDateTime = date1.getDatetime();
        LocalDateTime toDateTime = date2.getDatetime();


        LocalDateTime tempDateTime = LocalDateTime.from(fromDateTime);

        long years = tempDateTime.until(toDateTime, ChronoUnit.YEARS);
        tempDateTime = tempDateTime.plusYears(years);

        long months = tempDateTime.until(toDateTime, ChronoUnit.MONTHS);
        tempDateTime = tempDateTime.plusMonths(months);

        long days = tempDateTime.until(toDateTime, ChronoUnit.DAYS);
        tempDateTime = tempDateTime.plusDays(days);


        long hours = tempDateTime.until(toDateTime, ChronoUnit.HOURS);
        tempDateTime = tempDateTime.plusHours(hours);

        long minutes = tempDateTime.until(toDateTime, ChronoUnit.MINUTES);
        tempDateTime = tempDateTime.plusMinutes(minutes);

        long seconds = tempDateTime.until(toDateTime, ChronoUnit.SECONDS);

        Period period = Period.ZERO;
        period = period.plusYears(years);
        period = period.plusMonths(months);
        period = period.plusDays(days);
        Duration duration = Duration.ZERO;
        duration = duration.plusHours(hours);
        duration = duration.plusMinutes(minutes);
        duration = duration.plusSeconds(seconds);
        return new Timespan(duration,period);
    }


    static public Timespan calculateDifferenceBetweenDates(DateToken date1, DateToken date2) {
        Timespan timespan = new Timespan();
        timespan.setPeriod(Period.between(date1.getDate().toLocalDate(), date2.getDate().toLocalDate()));
        timespan.setTimespan(Duration.ZERO);
        return timespan;
    }

    // timespan ma 12 miesiecy po 30 dni
    static public Timespan operationOnTimespans(Timespan time1, Timespan time2, String operation) {
        switch (operation) {
            case "-": {


                int numberOfDays1 = time1.getPeriod().getYears() * 360 + time1.getPeriod().getMonths() * 30 + time1.getPeriod().getDays();
                int numberOfDays2 = time2.getPeriod().getYears() * 360 + time2.getPeriod().getMonths() * 30 + time2.getPeriod().getDays();
                int periodDays = Math.abs(numberOfDays1 - numberOfDays2);
                int numberOfYears = periodDays / 360;
                periodDays -= numberOfYears * 360;
                int numberOfMonths = periodDays / 30;
                periodDays -= numberOfMonths * 30;


                Period period = Period.ZERO;
                period = period.plusDays(periodDays);
                period = period.plusMonths(numberOfMonths);
                period = period.plusYears(numberOfYears);
                Duration duration = time1.getTimespan();
                duration = duration.minus(time2.getTimespan());
                if (time1.getTimespan().getSeconds() < time2.getTimespan().getSeconds()) {
                    period = period.minusDays(1);
                    duration = duration.abs();
                }
                return new Timespan(duration, period);

            }
            case "+": {
                Timespan result = new Timespan();
                Duration duration = time1.getTimespan();
                duration = duration.plusSeconds(time2.getTimespan().getSeconds());
                long Hours = time1.getTimespan().toHours() + time2.getTimespan().toHours();
                int daysToAdd = (int) Hours / 24;
                duration = duration.minusHours(daysToAdd * 24);
                Period period = time1.getPeriod();
                period = period.plusDays(time2.getPeriod().getDays() + daysToAdd);
                period = period.plusMonths(time2.getPeriod().getMonths());
                period = period.plusYears(time2.getPeriod().getYears());
                if (period.getMonths() > 12) {
                    int numberOfYearsToAdd = period.getMonths() / 12;
                    period = period.minusMonths(12 * numberOfYearsToAdd);
                    period = period.plusYears(numberOfYearsToAdd);
                }
                return new Timespan(duration, period);

            }
        }
        return null;
    }


    public static boolean isTimespanIsLonger(Timespan time1, Timespan time2) {
        Period period1 = time1.getPeriod();
        Period period2 = time2.getPeriod();
        if (period1.getYears() > period2.getYears()) return true;
        if (period1.getMonths() > period2.getMonths()) return true;
        if (period1.getDays() > period2.getDays()) return true;
        Duration duration1 = time1.getTimespan();
        Duration duration2 = time2.getTimespan();
        if (duration1.getSeconds() > duration2.getSeconds()) return true;
        return false;
    }


}
