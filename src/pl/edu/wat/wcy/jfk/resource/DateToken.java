package pl.edu.wat.wcy.jfk.resource;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateToken {

    private LocalDateTime date;

    private String format;


    //dodac do parametry konstruktora spllitera i bedzie uniwersalny konstrukor
    //dodac pole obslugujace wygodne dla nas formaty i dopisac metody zwracajace stringa w tych formatach i generujace LocalDate w tych formatach
    public DateToken(String token) {
        String[] splits = token.split("/");
        int day = Integer.parseInt(splits[0]);
        int month = Integer.parseInt(splits[1]);
        int year = Integer.parseInt(splits[2]);
        this.date = LocalDateTime.of(year,month,day,0,0,0);
    }

    public String printValidStringOfDate() {
        String day = String.valueOf(this.date.getDayOfMonth());
        String month = String.valueOf(this.date.getMonthValue());
        String year = String.valueOf(this.date.getYear());
        return day + "/" + month + "/" + year;
    }

    public DateToken(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
